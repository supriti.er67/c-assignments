/*3. Write a C program that will accept a hexadecimal number as input, and then display a menu
that will permit any of the following operations to be carried out
a) Display the hexadecimal equivalent of the one’s complement
b) Carry out a masking operation and then display the hexadecimal equivalent of the result
c) Carry out a bit shifting operation and then display the hexadecimal equivalent of the result
d) Exit
If the masking operation is selected, prompt the user for the type of operation (bitwise and,
bitwise exclusive or, or bitwise or) and then a (hexadecimal) value for the mask. If the bit
shifting operation is selected, prompt the user for the type of shift (left to right), and then the
number of bits.

*/

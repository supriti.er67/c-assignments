/*Write a program to count the vowels and letters in free text given as standard input. Read text a
character at a time until you encounter end-of-data. Then print out the number of occurrences
of each of the vowels a, e, i, o and u in the text, the total number of letters, and each of the vowels
as an integer percentage of the letter total.
Suggested output format is:
Numbers of characters:
a 3 ; e 2 ; i 0 ; o 1 ; u 0 ; rest 17
Percentages of total:
a 13%; e 8%; i 0%; o 4%; u 0%; rest 73%
Read characters to end of data using a construct such as
char ch;
while(( ch = getchar() ) >= ) {
 ch is the next character  ....
}
to read characters one at a time using getchar() until a negative value is returned.
*/

#include<stdio.h>
#include<stdlib.h>

int main()
{
	char ch;	
	FILE *fptr;
        float count_a=0.0,count_e=0.0,count_i=0.0,count_o=0.0,count_u=0.0,count_l=0.0;
		if((fptr=fopen("Mytext.txt","r"))==NULL)
		{
			printf("\nError in opening file\n");
			exit(1);
		}
			else
		{
			ch=fgetc(fptr);
			while(ch!=EOF)
			{
				
				switch(ch)
				{
					case 'a':case'A':
						count_a++;
						break;
					case 'e':case 'E': count_e++; break;
					case 'i':case 'I': count_i++; break;
					case 'o':case 'O': count_o++; break;
					case 'u':case 'U': count_u++; break;
					default: count_l++;
					break;

				}
				ch=getc(fptr);
			}
		printf("\ncount of vowels is:\n");
		printf("\na= %1.0f %\t",count_a);
		printf("\ne= %1.0f % \t",count_e);
		printf("\ni= %1.0f %\t",count_i);
		printf("\no= %1.0f %\t",count_o);
		printf("\nu= %1.0f %\t",count_u);
		printf("\nother= %1.0f %\t",count_l);	
	
		}
		fclose(fptr);
return 0;
}

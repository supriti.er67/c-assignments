/*files fputc use 
*/
#include<stdio.h>
#include<stdlib.h>

int main()
{
	FILE *fptr;
	int ch;
	if((fptr=fopen("myfile","r"))==NULL)
	{
		printf("\nError in opening file\n");	
		exit(1);
	}
	printf("Enter Text\n");
	/* Press ctrl+z/ctrl+d to stop reading characters*/
	while((ch=getchar())!=EOF)
		fputc(ch,fptr);
	fclose(fptr);

return 0;
}


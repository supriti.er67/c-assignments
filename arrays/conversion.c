/*convert number into binary,octal,hexadecimal
*/

#include<stdio.h>

void convert(int num,int b);
int main()
{
	int num;
	printf("\nenter decimal no.\n");
	scanf("%d",&num);
	printf("\nIn Binary:\t");	
	convert(num,2);
	printf("\nIn hexadecimal:\t");
	convert(num,16);
	printf("\n");
return 0;
}

void convert(int deci, int base)
{
	int i=0,j,rem;
	char arr[20];

	while(deci>0)
	{
		rem=deci%base;
		deci/=base;

		if(rem>9 && rem<16)
			arr[i++]=rem-10+'A';
		else
			arr[i++]=rem+'0';
	}
	for(j=i-1;j>=0;j--)
	printf("%c",arr[j]);
	
}

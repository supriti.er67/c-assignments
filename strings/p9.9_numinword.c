// wap to enter a 4 digit number and display it in words

#include<stdio.h>

int main()
{
	int n,num,d=0,dig[4];
	char *ones[]={"","one","two","three","four","five","six","seven","eight","nine","Ten"};

char *el[]={"Ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","Nineteen"};

char *tens[]={"","","Twenty","Thirty","fourty","fifty","sixty","seventy","eighty","ninety"};

printf("Enter a 4 digit number:");
scanf("%d",&num);

n=num;

	do
	{
		dig[d]=n%10;
		n/=10;
		d++;
	}while(n>0);

	if(d==4)
		printf("%s thousand\t",ones[dig[3]]);
	if(d>=3 && dig[2]!=0)
		printf("%s hundred\t",ones[dig[2]]);
	if(d>=2)
	{
		if(dig[1]==0)
		printf("%s\n",ones[dig[0]]);
		else if(dig[1]==1)
		printf("%s\n",el[dig[0]]);
		else
		printf("%s %s\n",tens[dig[1]],ones[dig[0]]);
	}
	if(d==1 && num!=0)
	printf("%s\n",ones[dig[0]]);
	if(num==0)
	printf("Zero\n");
return 0;
}

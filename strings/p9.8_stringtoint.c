// convert string to integer

#include<stdio.h>
#include<string.h>

int main()
{
	char str[20];
	int str_to_i(char str[]);

	printf("\nEnter a number\n");
	scanf("%s",str);
	printf("%d\n",str_to_i(str));
return 0;
}

int str_to_i(char str[])
{
	int i,num=0;
	if(str[0]=='-')
	 i=1;
	else
	i=0;
	while(i<strlen(str))
	num=num*10+(str[i++]-48);

	if(str[0]=='-')
	return -num;
	else
	return num;

}

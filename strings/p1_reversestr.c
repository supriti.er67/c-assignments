// reversing a string using macro

#include<stdio.h>

#define Reverse(funname,datatype)\
datatype funname (datatype var)\
	{	\
	{if((var==0)||(var=='\0')) {return var;}\
	else {var=funname((var+1));}\
	}\

Reverse(Revstr_fun,char*);
Reverse(Revnum_fun, int*);
int main()
{
	 char *str1="CAD";
	 int  num1=12389;
	int *p =&num1;
		
	printf("\nGiven string is %s\n",str1);
	str1=Revstr_fun(str1);
	printf("\nReversed string is %s\n",str1);
	
	printf("\nGiven number is %d\n",num1);
	p=Revstr_fun(p);
	printf("\nReversed number is %d\n",num1);

return 0;
}
